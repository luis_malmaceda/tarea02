package com.example.lab02

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.lab02.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    //Manipular Controles del layout Activity
    private lateinit var binding:ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Manipular Controles del layout Activity
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnVerificar.setOnClickListener {

            if(binding.edtAnio.text.toString().isEmpty()){
                Toast.makeText(this,"Ingresa año", Toast.LENGTH_SHORT).show()
                binding.edtAnio.requestFocus()
                return@setOnClickListener
            }
            val anio:Int =  binding.edtAnio.text.toString().toInt()

            when (anio){
                in 1994..2010 ->{
                    binding.tvPoblacion.setText("Población 7800000")
                    binding.imgRasgo.setImageResource(R.drawable.gen_z)

                }
                in 1981..1993 ->{
                    binding.tvPoblacion.setText("Población 7200000")
                    binding.imgRasgo.setImageResource(R.drawable.gen_y)
                }
                in 1969..1980 ->{
                    binding.tvPoblacion.setText("Población 9300000")
                    binding.imgRasgo.setImageResource(R.drawable.gen_x)
                }
                in 1949..1968 ->{
                    binding.tvPoblacion.setText("Población 12200000")
                    binding.imgRasgo.setImageResource(R.drawable.gen_ambicion)
                }
                in 1930..1948 ->{
                    binding.tvPoblacion.setText("Población 6300000")
                    binding.imgRasgo.setImageResource(R.drawable.gen_austeridad)
                }else->{
                    binding.tvPoblacion.setText("Sin Generación")
                }
            }

        }

    }

}